package me.bumblebeee_.skypeminecraft;

import com.samczsun.skype4j.Skype;
import com.samczsun.skype4j.SkypeBuilder;
import com.samczsun.skype4j.Visibility;
import com.samczsun.skype4j.chat.Chat;
import com.samczsun.skype4j.events.EventHandler;
import com.samczsun.skype4j.events.Listener;
import com.samczsun.skype4j.events.chat.message.MessageReceivedEvent;
import com.samczsun.skype4j.events.chat.user.UserAddEvent;
import com.samczsun.skype4j.events.chat.user.UserRemoveEvent;
import com.samczsun.skype4j.exceptions.ConnectionException;
import com.samczsun.skype4j.user.User;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class SkypeM {

    SkypeCommands sc = new SkypeCommands();

    static Skype skype;
    static Chat c = null;
    String user;
    String pass;

    public SkypeM(String user, String pass) {
        this.user = user;
        this.pass = pass;
    }

    public void login() {
        skype = new SkypeBuilder(user, pass).withAllResources().build();
        Bukkit.getServer().getLogger().info("[Skype] " + "Logging in..");
        try {
            skype.login();
        } catch (Exception e) {
            Bukkit.getServer().getLogger().warning("[Skype] " + "Failed to login!");
            e.printStackTrace();
        }
        try {
            skype.subscribe();
        } catch (ConnectionException e) {
            Bukkit.getServer().getLogger().warning("[Skype] " + "Failed to login!");
            e.printStackTrace();
        }
        Bukkit.getServer().getLogger().info("[Skype] " + "Successfully logged in!");
        try {
            skype.setVisibility(Visibility.ONLINE);
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }

    public void loadChat(String id) {
        try {
            c = skype.loadChat(id);
        } catch (Exception e) {
            Bukkit.getServer().getLogger().warning("[Skype] " + "Failed to load chat!");
            e.printStackTrace();
        }
    }

    public Chat getChat() {
        return c;
    }

    public void loadEvents() {
        loadMessageReceiveEvent();
        loadUserAddEvent();
        loadUserRemoveEvent();
    }

    public void loadUserAddEvent() {
        skype.getEventDispatcher().registerListener(new Listener() {
            @EventHandler
            public void onAdd(UserAddEvent e) throws ConnectionException {
                for (Player p : Bukkit.getServer().getOnlinePlayers()) {
                    p.sendMessage(ChatColor.WHITE + "[" + ChatColor.AQUA + "Skype" + ChatColor.WHITE +
                            "] " + e.getUser().getDisplayName() + " has joined.");
                }
                Bukkit.getServer().getLogger().info("[Skype] " + e.getUser().getDisplayName() + " has joined.");
            }
        });
    }

    public void loadUserRemoveEvent() {
        skype.getEventDispatcher().registerListener(new Listener() {
            @EventHandler
            public void onRemove(UserRemoveEvent e) throws ConnectionException {
                for (Player p : Bukkit.getServer().getOnlinePlayers()) {
                    p.sendMessage(ChatColor.WHITE + "[" + ChatColor.AQUA + "Skype" + ChatColor.WHITE +
                            "] " + e.getUser().getDisplayName() + " has left.");
                }
                Bukkit.getServer().getLogger().info("[Skype] " + e.getUser().getDisplayName() + " has left.");
            }
        });
    }

    public void loadMessageReceiveEvent() {
        skype.getEventDispatcher().registerListener(new Listener() {
            @EventHandler
            public void onMessage(MessageReceivedEvent e) throws ConnectionException {
                String message = e.getMessage().getContent().asPlaintext();
                User sender = e.getMessage().getSender();
                String msender =e.getMessage().getSender().getDisplayName();
                if (message.startsWith(".players") || message.startsWith(".who") || message.startsWith(".list")) {
                    sc.players(c);
                } else if (message.startsWith(".x")) {
                    if (!sc.isAdmin(sender.getUsername())) {
                        c.sendMessage("You do not have the required permissions!");
                    } else {
                        sc.adminCommand(c, sender, message);
                    }
                } else {
                    for (Player p : Bukkit.getServer().getOnlinePlayers()) {
                        p.sendMessage(ChatColor.WHITE + "[" + ChatColor.AQUA + "Skype" + ChatColor.WHITE +
                                "] " + msender + ": " + message);
                    }
                    Bukkit.getServer().getLogger().info("[Skype] " + e.getMessage().getSender().getDisplayName() + ": " + message);
                }
            }
        });
    }

    public void logout() {
        try {
            skype.logout();
            skype.setVisibility(Visibility.INVISIBLE);
            skype = null;
        } catch (ConnectionException e) {
            Bukkit.getServer().getLogger().warning("Failed to logout!");
            e.printStackTrace();
        }
    }

    public Skype getSkype() {
        return skype;
    }

}
