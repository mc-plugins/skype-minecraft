package me.bumblebeee_.skypeminecraft;

import com.samczsun.skype4j.Skype;
import com.samczsun.skype4j.chat.Chat;
import com.samczsun.skype4j.exceptions.ConnectionException;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class SkypeMinecraft extends JavaPlugin implements Listener {

    SkypeM sm;
    Skype skype;
    Chat c;

    static Plugin pl = null;

    public void onEnable() {
        pl = this;
        saveDefaultConfig();
        createSkype();
        Bukkit.getServer().getPluginManager().registerEvents(this, this);
    }

    public void onDisable() {
        if (sm.getSkype() != null) {
            Bukkit.getServer().getScheduler().runTaskAsynchronously(this, new Runnable() {
                @Override
                public void run() {
                    try {
                        c.sendMessage("Disabled Skype!");
                    } catch (ConnectionException e) {
                        e.printStackTrace();
                    }
                    sm.logout();
                }
            });
        }
    }

    public void createSkype() {
        if (getConfig().getString("chatID").equalsIgnoreCase("ChangeMe")) {
            Bukkit.getServer().getLogger().severe("******DISABLING SKYPE-MINECRAFT******");
            Bukkit.getServer().getLogger().severe("MAKE SURE CHATID IS SET IN THE CONFIG!");
            Bukkit.getServer().getPluginManager().disablePlugin(this);
            return;
        } else if (getConfig().getString("username").equalsIgnoreCase("ChangeMe")) {
            Bukkit.getServer().getLogger().severe("******DISABLING SKYPE-MINECRAFT******");
            Bukkit.getServer().getLogger().severe("MAKE SURE USERNAME AND PASSWORD IS SET IN THE CONFIG!");
            Bukkit.getServer().getPluginManager().disablePlugin(this);
        } else if (getConfig().getString("password").equalsIgnoreCase("ChangeMe")) {
            Bukkit.getServer().getLogger().severe("******DISABLING SKYPE-MINECRAFT******");
            Bukkit.getServer().getLogger().severe("MAKE SURE USERNAME AND PASSWORD IS SET IN THE CONFIG!");
            Bukkit.getServer().getPluginManager().disablePlugin(this);
        }
        sm = new SkypeM(getConfig().getString("username"), getConfig().getString("password"));
        Bukkit.getServer().getScheduler().runTaskAsynchronously(this, new Runnable() {
            @Override
            public void run() {
                sm.login();
                sm.loadChat(getConfig().getString("chatID"));
                c = sm.getChat();
                sm.loadEvents();
                try {
                    c.sendMessage("Enabled Skype!");
                } catch (ConnectionException e) {
                    e.printStackTrace();
                }
            }
        });
        skype = sm.getSkype();
    }

    public static Plugin getInstance() {
        return pl;
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Bukkit.getServer().getScheduler().runTaskAsynchronously(this, new Runnable() {
            @Override
            public void run() {
                try {
                    c.sendMessage(ChatColor.stripColor(e.getPlayer().getDisplayName()) + " joined the game.");
                } catch (ConnectionException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Bukkit.getServer().getScheduler().runTaskAsynchronously(this, new Runnable() {
            @Override
            public void run() {
                try {
                    c.sendMessage(ChatColor.stripColor(e.getPlayer().getDisplayName()) + " left the game.");
                } catch (ConnectionException e1) {
                    e1.printStackTrace();
                }
            }
        });
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent e) {
        if (c == null) return;
        Bukkit.getServer().getScheduler().runTaskAsynchronously(this, new Runnable() {
            @Override
            public void run() {
                try {
                    c.sendMessage(ChatColor.stripColor(e.getPlayer().getDisplayName()) + ": " + ChatColor.stripColor(e.getMessage()));
                } catch (ConnectionException e1) {
                    e1.printStackTrace();
                } catch (IllegalStateException e) {
                    createSkype();
                }
            }
        });
    }

    @EventHandler
    public void onConsoleSend(ServerCommandEvent e) {
        if (c == null) return;
        if (!e.getCommand().startsWith("say")) return;
        if (e.getCommand().startsWith("reload")) {
            if (getConfig().getString("safe-reload").equalsIgnoreCase("false")) {
                if (Bukkit.getServer().getOnlinePlayers().size() == 0) {
                    e.getSender().sendMessage(ChatColor.RED + "[Skype] Reloading with no players on can cause duplicate messages! " +
                            "to help fix this enable safe reload in the config!");
                }
            }
        }
        Bukkit.getServer().getScheduler().runTaskAsynchronously(this, new Runnable() {
            @Override
            public void run() {
                try {
                    c.sendMessage(e.getSender().getName() + ": " + ChatColor.stripColor(e.getCommand().replace("say ", "")));
                } catch (ConnectionException e1) {
                    Bukkit.getServer().getLogger().warning("Failed to send chat message to skype!");
                    e1.printStackTrace();
                }
            }
        });
    }
}
