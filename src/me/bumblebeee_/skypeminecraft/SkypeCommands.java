package me.bumblebeee_.skypeminecraft;

import com.samczsun.skype4j.chat.Chat;
import com.samczsun.skype4j.exceptions.ConnectionException;
import com.samczsun.skype4j.user.User;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class SkypeCommands {

    public void players(Chat c) throws ConnectionException {
        List<String> online = new ArrayList<>();
        String players = "null";
        for (Player p : Bukkit.getServer().getOnlinePlayers()) {
            online.add(p.getName());
            if (players.equals("null")) {
                players = p.getName();
            } else {
                players = players + ", " + p.getName();
            }
        }
        final String finalPlayers = players;
        Bukkit.getServer().getScheduler().runTaskAsynchronously(SkypeMinecraft.getInstance(), new Runnable() {
            @Override
            public void run() {
                try {
                    c.sendMessage("Online Players (" + online.size() + "):");
                    c.sendMessage(finalPlayers);
                } catch (ConnectionException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void adminCommand(Chat c, User sender, String message) throws ConnectionException {
        Bukkit.getServer().getScheduler().runTaskAsynchronously(SkypeMinecraft.getInstance(), new Runnable() {
            @Override
            public void run() {
                String cmd = message.replace(".x ", "");
                boolean r = Bukkit.getServer().dispatchCommand(Bukkit.getServer().getConsoleSender(), cmd);
                Bukkit.getServer().getLogger().info("[Skype] " + sender.getUsername() + " ran " + cmd);
                if (r) {
                    try {
                        c.sendMessage("Command ran.");
                    } catch (ConnectionException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        c.sendMessage("Invalid command!");
                    } catch (ConnectionException e) {
                        e.printStackTrace();
                    }

                }
            }
        });
    }

    public boolean isAdmin(String user) {
        List<String> adminsH = SkypeMinecraft.getInstance().getConfig().getStringList("admins");
        List<String> admins = new ArrayList<>();
        for (String s : adminsH) {
            admins.add(s.toLowerCase());
        }
        if (admins.contains(user)) {
            return true;
        }
        return false;
    }
}
